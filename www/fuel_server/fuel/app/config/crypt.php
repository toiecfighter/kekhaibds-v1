<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.2
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2019 Fuel Development Team
 * @link       https://fuelphp.com
 */

return array (	'legacy' => array (
	'crypto_key' => '-EwPIoyIknDwI60U3olzI17g',
	'crypto_iv' => 'pHYvksdto4yIISYJ4kw8MOPE',
	'crypto_hmac' => 'V3AtAst3YfQcSVcs04PxgFqc',
), 	'sodium' => array (
	'cipherkey' => 'b694f6069ab6931771d58f7655cc2bd3e270ce8de0e42b2f184e46275ef46a18',
),
);