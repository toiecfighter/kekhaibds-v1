<?php
/**
 * Use this file to override global defaults.
 *
 * See the individual environment DB configs for specific config information.
 */

return [
	// MySQL ドライバの設定
	'default' => [
		'type'           => 'pdo',
		'connection'     => [
			'dsn'            => 'mysql:host=fuel_mysql;dbname=fuel_db',
			'username'       => 'root',
			'password'       => 'fuel_db_password',
			'persistent'     => false,
			'compress'       => false,
		],
		'identifier'   => '`',
		'table_prefix'   => '',
		'charset'        => 'utf8',
		'enable_cache'   => true,
		'profiling'      => false,
	],
];