<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.2
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2019 Fuel Development Team
 * @link       https://fuelphp.com
 */


/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Api_Internal_Declares extends \Fuel\Core\Controller_Rest
{
	const GROUP_INVESTOR = 1;
	const GROUP_BXD = 2;
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */

	public function get_index()
	{
		if( ! Auth::check()){
			$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
		}else{
			$param = \Input::get();
			$investor = Model_Investor::find_by_user_id(Auth::get_user_id()[1]);
			$investor_id = null;
			if($investor){
				$investor_id = $investor->id;
			}
			$data = Model_Investor_Declaration::get_declare($investor_id,$param);
			$this->response($data);
		}
	}

	public function post_index()
	{

	}
}
