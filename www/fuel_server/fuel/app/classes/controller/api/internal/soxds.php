<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.2
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2019 Fuel Development Team
 * @link       https://fuelphp.com
 */


/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Api_Internal_Soxds extends \Fuel\Core\Controller_Rest
{
	const GROUP_INVESTOR = 1;
	const GROUP_BXD = 2;
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */

	public function get_index(){
		if( ! Auth::check()){
			$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
		}else{
			$list_soxd = Model_Soxd::get_list_data();
			$this->response($list_soxd);
		}
	}

	public function post_index()
	{
		if( ! Auth::check()){
			$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
		}else{
			$params = \Input::post();
//			var_dump($params);die();
			\Fuel\Core\DB::start_transaction();
			$user_id = Auth::create_user(
				$params['user_name'],
				$params['pass_word'],
				$params['email'],
				3,
				array(
					'fullname' => 'A. New User SoXD',
				)
			);
			$soxd_orm = Model_Soxd::forge();
			$soxd_orm->name = $params['name'];
			$soxd_orm->city = $params['city'];
			$soxd_orm->user_id = $user_id;
			$soxd_orm->active = 1;
			$soxd_orm->address = $params['address'];
			$soxd_orm->city = $params['city'];
			$soxd_orm->save();
			\Fuel\Core\DB::commit_transaction();
			$this->response($soxd_orm);

		}
	}

}
