<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.2
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2019 Fuel Development Team
 * @link       https://fuelphp.com
 */


/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Api_Internal_Investors extends \Fuel\Core\Controller_Rest
{
	const GROUP_INVESTOR = 1;
	const GROUP_BXD = 2;
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */

	public function get_bds()
	{
		if( ! Auth::check()){
			$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
		}else{
			$data = [];
			$data['realestate'] = Model_Realestate::find('all');
			$data['brand'] = Model_Brand::find('all');
			$data['variety'] = Model_Variety::find('all');
			$this->response($data);
		}
	}

	public function get_index(){
		if( ! Auth::check()){
			$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
		}else{
			$user_id = Auth::get_user_id()[1];
			$soxd = Model_Soxd::find_by_user_id($user_id);
			$data = Model_Investor::get_list_data($soxd->city);
			$this->response($data);
		}
	}

	public function get_projects()
	{

		if( ! Auth::check()){
			$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
		}else{
			$options = \Input::get();
			$investor = Model_Investor::find_by_user_id(Auth::get_user_id()[1]);
			if($investor)
				$options['investor_id'] = $investor->id;
			$projects = Model_Project::get_project_list($options);
			$this->response($projects);
		}

	}

	public function post_account(){
		if( ! Auth::check()){
			$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
		}else{
			$params = \Input::post();
//			var_dump($params);die();
			\Fuel\Core\DB::start_transaction();
			$user_id = Auth::create_user(
				$params['user_name'],
				$params['pass_word'],
				$params['email'],
				1,
				array(
					'fullname' => 'A. New User SoXD',
				)
			);
			\Fuel\Core\DB::commit_transaction();
			\Fuel\Core\DB::start_transaction();
			$soxd_orm = Model_Investor::forge();
			$soxd_orm->level = 1;
			$soxd_orm->name = $params['name'];
			$soxd_orm->city = $params['city'];
			$soxd_orm->email = $params['email'];
			$soxd_orm->user_id = $user_id;
			$soxd_orm->active = 1;
			$soxd_orm->type = 'admin';
			$soxd_orm->address = $params['address'];
			$soxd_orm->tax_code = $params['tax_code'];
			$soxd_orm->save();
			\Fuel\Core\DB::commit_transaction();
			$this->response($soxd_orm);
		}
	}
	public function post_index()
	{
		if( ! Auth::check()){
			$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
		}else{
			$params = \Input::post();
			$project_info = json_decode($params['project_info'], 1);
			$data = json_decode($params['row_data'], 1);
			$investor = Model_Investor::find_by_user_id(Auth::get_user_id()[1]);
			$project_id = Model_Project::create_or_update_project($project_info, $investor->id);
			Model_Investor_Declaration::insert_declares($data, $investor->id, $project_id);
			$this->response($data);
		}

	}

}
