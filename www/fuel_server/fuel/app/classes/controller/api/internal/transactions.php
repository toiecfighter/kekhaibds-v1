<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.2
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2019 Fuel Development Team
 * @link       https://fuelphp.com
 */


/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Api_Internal_Transactions extends \Fuel\Core\Controller_Rest
{
	const GROUP_INVESTOR = 1;
	const GROUP_BXD = 2;
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */

	public function get_index()
	{
		if( ! Auth::check()){
			$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
		}else{
			$params = \Input::get();
			$investor = Model_Investor::find_by_user_id(Auth::get_user_id()[1]);
			if($investor)
				$params['investor_id'] = $investor->id;
			$trans = Model_Transaction::get_transaction($params);
			$this->response($trans);
		}

	}

	public function post_index()
	{
		if( ! Auth::check()){
			$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
		}else{
			$investor = Model_Investor::find_by_user_id(Auth::get_user_id()[1]);
			$params = \Input::post();
			$is_update = false;
			if(isset($params['is_update'])){
				$is_update = true;
			}
			Model_Transaction::create_transaction(json_decode($params['row_data'], 1), $params['project_id'], $investor->id, $is_update);
			$this->response($params);
		}
	}
}
