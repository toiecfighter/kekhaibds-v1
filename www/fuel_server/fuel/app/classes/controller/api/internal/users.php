<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.2
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2019 Fuel Development Team
 * @link       https://fuelphp.com
 */


/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Api_Internal_Users extends \Fuel\Core\Controller_Rest
{
	const GROUP_INVESTOR = 1;
	const GROUP_BXD = 2;
	const GROUP_SOXD = 3;
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */

	public function before()
	{
		parent::before();
		// check for admin
	}

	public function get_create_account(){
		Auth::create_user(
			'admin_bxd',
			'123456',
			'admin-bxd@example.org',
			2,
			array(
				'fullname' => 'Admin Bo Xay Dung',
			)
		);
	}

	public function post_login(){

		if (\Input::method() == 'POST')
		{
			// check the credentials.
			if (\Auth::instance()->login(\Input::param('user_name'), \Input::param('pass_word')))
			{
				$user_group  = Auth::get('group');
				switch ((int)$user_group) {
					case self::GROUP_INVESTOR:
						// get investor
						$investor = Model_Investor::find_by_user_id(Auth::get_user_id()[1]);
						$investor->group = self::GROUP_INVESTOR;
						$this->response($investor);
						break;
					case self::GROUP_BXD:
						$this->response(['group' => self::GROUP_BXD]);
						break;
					case self::GROUP_SOXD:
						$user_id = Auth::get_user_id();
						$soxd = Model_Soxd::find_by_user_id($user_id[1]);
						$this->response(['group' => self::GROUP_SOXD, 'city'=>$soxd->city, 'name' => $soxd->name]);
						break;
					default:
						break;
				}
			}
			else
			{
				$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
			}
		}
	}

	public function post_register(){
		if (\Input::method() == 'POST')
		{
			// check the credentials.
			if (\Auth::instance()->login(\Input::param('user_name'), \Input::param('pass_word')))
			{
				$member_group = Auth::get('group');
				Auth::create_user(
					'user_name',
					'pass_word',
					'anewuser@example.org',
					$member_group,
					array(
						'fullname' => 'A. New User',
					)
				);
				// get user
				$user_id = Auth::get_user_id();
				if((int)$member_group == self::GROUP_INVESTOR){
				}
			}
			else
			{
				$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
			}
		}
	}


	public function post_log_out()
	{

	}


	public function get_log_out(){
		Auth::logout();
	}

	public function action_index()
	{
		var_dump([1,2,3,4,5,6]);
		return Response::forge(View::forge('welcome/index'));
	}

	/**
	 * A typical "Hello, Bob!" type example.  This uses a Presenter to
	 * show how to use them.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_hello()
	{
		return Response::forge(Presenter::forge('welcome/hello'));
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(Presenter::forge('welcome/404'), 404);
	}
}
