<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.2
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2019 Fuel Development Team
 * @link       https://fuelphp.com
 */


/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Controller_Api_Internal_Projects extends \Fuel\Core\Controller_Rest
{
	const GROUP_INVESTOR = 1;
	const GROUP_BXD = 2;
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function get_declare_data(){
		$option = \Input::get();
		$project_info = Model_Project::find_by_id($option['project_id']);
		$response_data = [];
		$option['investor_id'] = $project_info->investor_id;
		$parent = [0,0,0,0,0,0];

		$val = static::handle_data(1,1,1,$option);
		$response_data['val_1'] = $val;
		$parent = static::get_parent_all($parent, $val);
		$val = static::handle_data(1,2,1,$option);
		$response_data['val_2'] = $val;
		$parent = static::get_parent_all($parent, $val);
		$val = static::handle_data(1,3,1,$option);
		$response_data['val_3'] = $val;
		$parent = static::get_parent_all($parent, $val);
		$response_data['parent'] = $parent;
		$this->response($response_data);

	}

	public function get_excel()
	{
		$params = \Input::get();
		$option = json_decode($params['params'], true);
		$option['project_id'] = $params['project_id'];
		$project_info = Model_Project::find_by_id($option['project_id']);
		$investor = Model_Investor::find_by_id($project_info->investor_id);
		$template_7a = DOCROOT.'excel/7a_template.xlsx';
		$copied = DOCROOT.'excel/7a.xlsx';

		$exists = \Fuel\Core\File::exists($copied);
		if($exists){
			\Fuel\Core\File::delete($copied);
		}
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_7a);
		$worksheet = $spreadsheet->getActiveSheet();

		// investor info
		$worksheet->getCell('D6')->setValue($investor->name);
		$worksheet->getCell('D7')->setValue($investor->tax_code);
		$worksheet->getCell('D8')->setValue($investor->address . ' '. $investor->city);
		$worksheet->getCell('D9')->setValue($investor->email);
		$worksheet->getCell('D10')->setValue($project_info->code);
		$worksheet->getCell('D11')->setValue($project_info->scale);
		$worksheet->getCell('D12')->setValue($project_info->total_investment);
		$worksheet->getCell('D13')->setValue($project_info->start_date.' - '.$project_info->end_date);
		if($project_info->type_of_investment == 1){
			$worksheet->getCell('D17')->setValue('X');
		}elseif($project_info->type_of_investment == 2){
			$worksheet->getCell('D18')->setValue('X');
		}
		else{
			$worksheet->getCell('D19')->setValue('X');
		}
		$parent = [0,0,0,0,0,0];
		$option['investor_id'] = $project_info->investor_id;
		$val = static::handle_data(1,1,1,$option);
		$parent = static::get_parent_all($parent, $val);
		$worksheet->getCell('D28')->setValue($val[0]);
		$worksheet->getCell('E28')->setValue($val[1]);
		$worksheet->getCell('F28')->setValue($val[2]);
		$worksheet->getCell('G28')->setValue($val[3]);
		$worksheet->getCell('H28')->setValue($val[4]);
		$worksheet->getCell('I28')->setValue($val[5]);
		$val = static::handle_data(1,2,1,$option);
		$parent = static::get_parent_all($parent, $val);
		$worksheet->getCell('D29')->setValue($val[0]);
		$worksheet->getCell('E29')->setValue($val[1]);
		$worksheet->getCell('F29')->setValue($val[2]);
		$worksheet->getCell('G29')->setValue($val[3]);
		$worksheet->getCell('H29')->setValue($val[4]);
		$worksheet->getCell('I29')->setValue($val[5]);
		$val = static::handle_data(1,3,1,$option);
		$parent = static::get_parent_all($parent, $val);
		$worksheet->getCell('D30')->setValue($val[0]);
		$worksheet->getCell('E30')->setValue($val[1]);
		$worksheet->getCell('F30')->setValue($val[2]);
		$worksheet->getCell('G30')->setValue($val[3]);
		$worksheet->getCell('H30')->setValue($val[4]);
		$worksheet->getCell('I30')->setValue($val[5]);

		$worksheet->getCell('D26')->setValue($parent[0]);
		$worksheet->getCell('E26')->setValue($parent[1]);
		$worksheet->getCell('F26')->setValue($parent[2]);
		$worksheet->getCell('G26')->setValue($parent[3]);
		$worksheet->getCell('H26')->setValue($parent[4]);
		$worksheet->getCell('I26')->setValue($parent[5]);


		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save(DOCROOT.'excel/7a.xlsx');
		\Fuel\Core\File::download($copied);
	}


	public static function get_parent_all($parent, $val){
		$parent[0] += $val[0];
		$parent[1] += $val[1];
		$parent[2] += $val[2];
		$parent[3] += $val[3];
		$parent[4] += $val[4];
		$parent[5] += $val[5];
		return $parent;
	}
	public function handle_data($rst_id, $br_id, $var_id, $option){
		$v1 = [
			'real_estate_id' => $rst_id,
			'variety_id'=> $br_id,
			'brand_id'=> $var_id
		];
		$val_1_1_1 = [0,0,0,0,0,0];
		$val_1_1_1_temp = Model_Investor_Declaration::get_declare($option['investor_id'], array_merge($option, $v1));
		$trans_v1 = Model_Transaction::handle_data($rst_id,$br_id,$var_id, $option);
		foreach ($val_1_1_1_temp as $val){
			$val_1_1_1[0] += (int)$val['apr_room'];
			$val_1_1_1[1] += (int)$val['apr_square'];
			$val_1_1_1[2] += (int)$val['eligible_room'];
			$val_1_1_1[3] += (int)$val['eligible_square'];
		}
		$val_1_1_1[4] = $val_1_1_1[2]-$trans_v1[0];
		$val_1_1_1[5] = $val_1_1_1[3]-$trans_v1[1];
		return $val_1_1_1;
	}

	public function get_excel_7c()
	{
		$params = \Input::get();
		$option = json_decode($params['params'], true);
		$option['project_id'] = $params['project_id'];
		$project_info = Model_Project::find_by_id($option['project_id']);
		$investor = Model_Investor::find_by_id($project_info->investor_id);
		$template_7a = DOCROOT.'excel/7c_template.xlsx';
		$copied = DOCROOT.'excel/7c.xlsx';

		$exists = \Fuel\Core\File::exists($copied);
		if($exists){
			\Fuel\Core\File::delete($copied);
		}
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_7a);
		$worksheet = $spreadsheet->getActiveSheet();

		// investor info
		$worksheet->getCell('F6')->setValue($investor->name);
		$worksheet->getCell('F7')->setValue($investor->tax_code);
		$worksheet->getCell('F8')->setValue($investor->address . ' '. $investor->city);
		$worksheet->getCell('F9')->setValue($investor->email);
		$worksheet->getCell('F10')->setValue($project_info->code);
		$worksheet->getCell('F11')->setValue($project_info->scale);
		$worksheet->getCell('F12')->setValue($project_info->total_investment);
		$worksheet->getCell('F13')->setValue($project_info->start_date.' - '.$project_info->end_date);
		if($project_info->type_of_investment == 1){
			$worksheet->getCell('D17')->setValue('X');
		}elseif($project_info->type_of_investment == 2){
			$worksheet->getCell('D18')->setValue('X');
		}
		else{
			$worksheet->getCell('D19')->setValue('X');
		}

		$option['investor_id'] = $project_info->investor_id;
		$trans = Model_Transaction::get_tran_by_option(1,1,1, $option);
		$position = static::insert_data($worksheet, $trans, 29);
		$trans = Model_Transaction::get_tran_by_option(1,2,1, $option);
		$position = static::insert_data($worksheet, $trans, $position + 1);
		$trans = Model_Transaction::get_tran_by_option(1,3,1, $option);
		$position = static::insert_data($worksheet, $trans, $position + 1);
		$worksheet->getCell('F26')->setValue("=SUM(F27:F{$position})");
		$worksheet->getCell('G26')->setValue("=SUM(G27:G{$position})");
		$worksheet->getCell('H26')->setValue("=SUM(H27:H{$position})");
		$worksheet->getCell('I26')->setValue("=SUM(I27:I{$position})");
		$worksheet->getCell('J26')->setValue("=SUM(J27:J{$position})");
		$worksheet->getCell('K26')->setValue("=SUM(K27:K{$position})");
		$worksheet->getCell('L26')->setValue("=SUM(L27:L{$position})");
		$worksheet->getCell('M26')->setValue("=SUM(M27:M{$position})");
//		var_dump($trans);die();

//		$parent = [0,0,0,0];
//		$option['investor_id'] = $project_info->investor_id;
//		$val = static::handle_data(1,1,1,$option);
//		$parent = static::get_parent_all($parent, $val);
//		$worksheet->getCell('D28')->setValue($val[0]);
//		$worksheet->getCell('E28')->setValue($val[1]);
//		$worksheet->getCell('F28')->setValue($val[2]);
//		$worksheet->getCell('G28')->setValue($val[3]);
//		$val = static::handle_data(1,2,1,$option);
//		$parent = static::get_parent_all($parent, $val);
//		$worksheet->getCell('D29')->setValue($val[0]);
//		$worksheet->getCell('E29')->setValue($val[1]);
//		$worksheet->getCell('F29')->setValue($val[2]);
//		$worksheet->getCell('G29')->setValue($val[3]);
//		$val = static::handle_data(1,3,1,$option);
//		$worksheet->getCell('D30')->setValue($val[0]);
//		$worksheet->getCell('E30')->setValue($val[1]);
//		$worksheet->getCell('F30')->setValue($val[2]);
//		$worksheet->getCell('G30')->setValue($val[3]);
//		$parent = static::get_parent_all($parent, $val);
//		$worksheet->getCell('D26')->setValue($parent[0]);
//		$worksheet->getCell('E26')->setValue($parent[1]);
//		$worksheet->getCell('F26')->setValue($parent[2]);
//		$worksheet->getCell('G26')->setValue($parent[3]);


		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save(DOCROOT.'excel/7c.xlsx');
		\Fuel\Core\File::download($copied);
	}
	public static function insert_data($worksheet, $trans, $position){
		$cell_point = $position;
		$worksheet->insertNewRowBefore($position, count($trans));
		foreach ($trans as $index => $tran){
			$worksheet->getCell('D'.$cell_point)->setValue($tran['buyer']);
			$worksheet->getCell('E'.$cell_point)->setValue($tran['code']);
			if($tran['type'] == '1'){
				$worksheet->getCell('F'.$cell_point)->setValue($tran['room']);
				$worksheet->getCell('G'.$cell_point)->setValue($tran['square']);
				$worksheet->getCell('H'.$cell_point)->setValue($tran['price']);
				$total = (int)$tran['square']*(int)$tran['price'] + (int)$tran['room']*(int)$tran['price'];
				$worksheet->getCell('I'.$cell_point)->setValue($total);
			}else{
				$worksheet->getCell('J'.$cell_point)->setValue($tran['room']);
				$worksheet->getCell('K'.$cell_point)->setValue($tran['square']);
				$worksheet->getCell('L'.$cell_point)->setValue($tran['price']);
				$total = (int)$tran['square']*(int)$tran['price'] + (int)$tran['room']*(int)$tran['price'];
				$worksheet->getCell('M'.$cell_point)->setValue($total);
			}
			$cell_point = $position + $index + 1;
		}
		return $position + count($trans);
	}

	public function post_index()
	{

	}
	public function get_item(){
		if( ! Auth::check()){
			$this->response(array('status'=> 0, 'error'=> 'Not Authorized'), 401);
		}else{
			$params = \Input::get();
			$data['info'] = Model_Project::find_by_id($params['project_id']);
			// get declare
			$data['declares'] = Model_Investor_Declaration::find('all', array(
				'where' => array(
					array('project_id', $params['project_id']),
				),
			));
			$this->response($data);
		}

	}
}
