<?php

use Orm\Model;

class Model_Investor_Declaration extends Model {
	protected static $_table_name = 'invester_declaration';
	protected static $_properties = array(
		'id'					=> array('data_type' => 'serial',	'label' => 'id'),
		'investor_id'			=> array('data_type' => 'integer',		'label' => 'name'),
		'apr_room'				=> array('data_type' => 'integer',	'label' => 'country_id'),
		'apr_square'			=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'eligible_room'			=> array('data_type' => 'integer',	'label' => 'country_id'),
		'eligible_square'		=> array('data_type' => 'integer',	'label' => 'country_id'),
		'real_estate_id'		=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'variety_id'			=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'brand_id'				=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'quarter'				=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'year'					=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'status'				=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'project_id'			=> array('data_type' => 'integer',	'label' => 'apr_square'),
	);
	public static function insert_declares($data, $investor_id, $project_id)
	{
		\Fuel\Core\DB::start_transaction();
		$dels = static::find('all', array('where' => array('project_id' => $project_id)));
		foreach ($dels as $del){
			$del->delete();
		}

		// delete all row of project id

		foreach($data as $dat){
			$orm_de = static::forge();
			$orm_de->investor_id = $investor_id;
			$orm_de->project_id = $project_id;
			$orm_de->apr_room = $dat['apr_room'] ?: null;
			$orm_de->apr_square = $dat['apr_square'] ?: null;
			$orm_de->eligible_room = $dat['eligible_room'] ?:  null;
			$orm_de->eligible_square = $dat['eligible_square'] ?: null;
			$orm_de->real_estate_id = $dat['real_estate_id'] ?? null;
			$orm_de->variety_id = $dat['variety_id'] ?? null;
			$orm_de->brand_id = $dat['brand_id'] ?? null;
			$orm_de->quarter = $dat['quarter'] ?? null;
			$orm_de->year = $dat['year'] ?? null;
			$orm_de->status = 1;
			$orm_de->save();
		}

		\Fuel\Core\DB::commit_transaction();
	}

	public static function get_declare($investor_id, $options){
		$query = DB::select('*')
			->from(array(static::table(), 'de'))
			->join(array('info_project', 'pr'))
			->on('de.project_id', '=', 'pr.id');
		if($investor_id){
			$query->where('pr.investor_id', $investor_id);
		}
		if(isset($options['quarter']) && $options['quarter']){
			$query->where('de.quarter', $options['quarter']);
		}
		if(isset($options['year']) && $options['quarter']){
			$query->where('de.year', $options['year']);
		}
		if(isset($options['city']) && $options['city']){
			$query->join(array('investor', 'iv'))
				->on('de.investor_id', '=', 'iv.id')->where('iv.city', $options['city']);
		}
		if(isset($options['project_id']) && $options['project_id']){
			$query->where('de.project_id', $options['project_id']);
		}
		if(isset($options['real_estate_id']) && $options['real_estate_id']){
			$query->where('de.real_estate_id', $options['real_estate_id']);
		}
		if(isset($options['variety_id']) && $options['variety_id']){
			$query->where('de.variety_id', $options['variety_id']);
		}
		if(isset($options['brand_id']) && $options['brand_id']){
			$query->where('de.brand_id', $options['brand_id']);
		}
		return $query->execute()->as_array();
	}

}