<?php

use Orm\Model;

class Model_Soxd extends Model {
	protected static $_table_name = 'soxd';
	const TYPE_ADMIN = 'admin';
	const TYPE_MEMBER = 'member';
	protected static $_properties = array(
		'id'					=> array('data_type' => 'serial',	'label' => 'id'),
		'name'					=> array('data_type' => 'text',		'label' => 'name'),
		'user_id'				=> array('data_type' => 'integer',	'label' => 'country_id'),
		'active'				=> array('data_type' => 'smallint',	'label' => 'active', 'default' => 1),
		'type'					=> array('data_type' => 'text',		'label' => 'watermark'),
		'address'				=> array('data_type' => 'text',		'label' => 'watermark'),
		'city'				=> array('data_type' => 'text',		'label' => 'watermark'),
	);

	public static function get_list_data(){
		return DB::select('soxd.*','us.username')
			->from(array(static::table(), 'soxd'))
			->join(array('users', 'us'))
			->on('us.id', '=', 'soxd.user_id')
			->execute()->as_array();
	}

}