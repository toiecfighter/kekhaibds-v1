<?php

use Orm\Model;

class Model_Transaction extends Model {
	protected static $_table_name = 'transaction';
	protected static $_properties = array(
		'id'					=> array('data_type' => 'serial',	'label' => 'id'),
		'buyer'					=> array('data_type' => 'text',		'label' => 'name'),
		'code'					=> array('data_type' => 'text',	'label' => 'country_id'),
		'real_estate_id'		=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'variety_id'			=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'brand_id'				=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'quarter'				=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'square'				=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'price'				=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'room'				=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'year'					=> array('data_type' => 'integer',	'label' => 'apr_square'),
		'investor_id'			=> array('data_type' => 'int',		'label' => 'level'),
		'project_id'			=> array('data_type' => 'int',		'label' => 'level'),
		'type'					=> array('data_type' => 'int',		'label' => 'level'),
	);

	public static function create_transaction($data, $project_id, $investor_id, $is_update = false)
	{
		\Fuel\Core\DB::start_transaction();
		// delete old row
		if($is_update){
			$dels = static::find('all', array('where' => array('project_id' => $project_id)));
			foreach ($dels as $del){
				$del->delete();
			}
		}

		foreach($data as $dat){
			$orm_tran = static::forge();
			$orm_tran->investor_id = $investor_id;
			$orm_tran->project_id = $project_id;
			$orm_tran->buyer = $dat['buyer'] ?: null;
			$orm_tran->type = $dat['type'] ?: 1;

			$orm_tran->square = $dat['square'] ?:  0;
			$orm_tran->price = $dat['price'] ?: 0;
			$orm_tran->room = $dat['room'] ?: 0;

			$orm_tran->real_estate_id = $dat['real_estate_id'] ?? null;
			$orm_tran->variety_id = $dat['variety_id'] ?? null;
			$orm_tran->brand_id = $dat['brand_id'] ?? null;
			$orm_tran->quarter = $dat['quarter'] ?? null;
			$orm_tran->year = $dat['year'] ?? null;
			$orm_tran->save();
		}
		\Fuel\Core\DB::commit_transaction();

	}
	public static function get_transaction($options)
	{
		$query = DB::select('tran.*')
			->from(array(static::table(), 'tran'));
		if(isset($options['investor_id'])){
			$query->where('tran.investor_id', $options['investor_id']);
		}
		if(isset($options['quarter']) && $options['quarter']){
			$query->where('tran.quarter', $options['quarter']);
		}
		if(isset($options['year']) && $options['quarter']){
			$query->where('tran.year', $options['year']);
		}
		if(isset($options['project_id']) && $options['project_id']) {
			$query->where('tran.project_id', $options['project_id']);
		}
		if(isset($options['city']) && $options['city']){
			$query->join(array('investor', 'iv'))
				->on('tran.investor_id', '=', 'iv.id')->where('iv.city', $options['city']);
		}
		if(isset($options['real_estate_id']) && $options['real_estate_id']){
			$query->where('tran.real_estate_id', $options['real_estate_id']);
		}
		if(isset($options['variety_id']) && $options['variety_id']){
			$query->where('tran.variety_id', $options['variety_id']);
		}
		if(isset($options['brand_id']) && $options['brand_id']){
			$query->where('tran.brand_id', $options['brand_id']);
		}
		return $query->execute()->as_array();
	}
	public static function handle_data($rst_id, $br_id, $var_id, $option){
		$v1 = [
			'real_estate_id' => $rst_id,
			'variety_id'=> $br_id,
			'brand_id'=> $var_id
		];
		$val_1_1_1 = [0,0];
		$val_1_1_1_temp = static::get_transaction(array_merge($option, $v1));
		foreach ($val_1_1_1_temp as $val){
			$val_1_1_1[0] += (int)$val['room'];
			$val_1_1_1[1] += (int)$val['square'];
		}
		return $val_1_1_1;
	}

	public static function get_tran_by_option($rst_id, $br_id, $var_id, $option){
		$v1 = [
			'real_estate_id' => $rst_id,
			'variety_id'=> $br_id,
			'brand_id'=> $var_id
		];
		return static::get_transaction(array_merge($option, $v1));
	}
}