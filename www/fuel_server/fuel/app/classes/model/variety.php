<?php

use Orm\Model;

class Model_Variety extends Model {
	protected static $_table_name = 'variety';
	protected static $_properties = array(
		'id'					=> array('data_type' => 'serial',	'label' => 'id'),
		'type'					=> array('data_type' => 'text',		'label' => 'name'),
		'name'					=> array('data_type' => 'text',		'label' => 'name'),
	);
}