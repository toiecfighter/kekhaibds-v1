<?php

use Orm\Model;

class Model_Project extends Model {
	protected static $_table_name = 'info_project';
	protected static $_properties = array(
		'id'					=> array('data_type' => 'serial',	'label' => 'id'),
		'name'					=> array('data_type' => 'text',		'label' => 'name'),
		'code'					=> array('data_type' => 'text',	'label' => 'country_id'),
		'location'				=> array('data_type' => 'text',		'label' => 'watermark'),
		'total_investment'		=> array('data_type' => 'int',		'label' => 'watermark'),
		'start_date'			=> array('data_type' => 'text',		'label' => 'watermark'),
		'end_date'				=> array('data_type' => 'text',		'label' => 'watermark'),
		'scale'					=> array('data_type' => 'int',		'label' => 'level'),
		'type_of_investment'	=> array('data_type' => 'int',		'label' => 'level'),
		'investor_id'			=> array('data_type' => 'int',		'label' => 'level'),
	);

	public static function create_or_update_project($project, $investor_id)
	{
		\Fuel\Core\DB::start_transaction();
		if(isset($project['project_id'])){
			$project_orm = static::find_by_id($project['project_id']);
		}else{
			// create
			$project_orm = static::forge();
		}
		$project_orm->name = $project['name'] ?? '';
		$project_orm->code = $project['code'] ?? '';
		$project_orm->total_investment = $project['total_investment'] ?? '';
		$project_orm->start_date = $project['start_date'] ?? '';
		$project_orm->end_date = $project['end_date'] ?? '';
		$project_orm->type_of_investment = $project['type_of_investment'] ?? '';
		$project_orm->scale = $project['scale'] ?? '';
		$project_orm->investor_id = $investor_id;
		$project_orm->save();
		\Fuel\Core\DB::commit_transaction();
		return $project_orm->id;

	}
	public static function get_project_list($options)
	{
		$query = DB::select('de.*')
			->from(array(static::table(), 'de'));
		if(isset($options['investor_id'])){
			$query->where('de.investor_id', $options['investor_id']);
		}
		if(isset($options['quarter']) && $options['quarter']){
			$query->where('de.quarter', $options['quarter']);
		}
		if(isset($options['year']) && $options['quarter']){
			$query->where('de.year', $options['year']);
		}
		if(isset($options['city']) && $options['city']){
			$query->join(array('investor', 'iv'))
				->on('de.investor_id', '=', 'iv.id')->where('iv.city', $options['city']);
		}
		return $query->execute()->as_array();
	}
}