<?php

use Orm\Model;

class Model_Realestate extends Model {
	protected static $_table_name = 'realestate';
	protected static $_properties = array(
		'id'					=> array('data_type' => 'serial',	'label' => 'id'),
		'type'					=> array('data_type' => 'text',		'label' => 'name'),
		'name'					=> array('data_type' => 'text',		'label' => 'name'),
	);
}