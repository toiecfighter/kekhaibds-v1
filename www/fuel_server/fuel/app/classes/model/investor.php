<?php

use Orm\Model;

class Model_Investor extends Model {
	protected static $_table_name = 'investor';
	const TYPE_ADMIN = 'admin';
	const TYPE_MEMBER = 'member';
	protected static $_properties = array(
		'id'					=> array('data_type' => 'serial',	'label' => 'id'),
		'name'					=> array('data_type' => 'text',		'label' => 'name'),
		'user_id'				=> array('data_type' => 'integer',	'label' => 'country_id'),
		'active'				=> array('data_type' => 'smallint',	'label' => 'active', 'default' => 1),
		'type'					=> array('data_type' => 'text',		'label' => 'watermark'),
		'email'					=> array('data_type' => 'text',		'label' => 'watermark'),
		'level'					=> array('data_type' => 'int',		'label' => 'level'),
		'tax_code'				=> array('data_type' => 'text',		'label' => 'watermark'),
		'address'				=> array('data_type' => 'text',		'label' => 'watermark'),
		'city'				=> array('data_type' => 'text',		'label' => 'watermark'),
	);
	public static function get_investor_by_user_id($user_id)
	{

	}
	public static function get_list_data($city){
		return DB::select('investor.*','us.username')
			->from(array(static::table(), 'investor'))
			->join(array('users', 'us'))
			->on('us.id', '=', 'investor.user_id')
			->where('investor.city', '=', $city)
			->execute()->as_array();
	}

}