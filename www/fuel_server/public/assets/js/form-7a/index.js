const GROUP_INVESTOR = 1
const GROUP_BXD = 2
var myApp = angular.module('form7aModule', [])
myApp.controller('form7aController', form7aController)
form7aController.$inject = ['$scope', '$http', '$compile', '$filter']
function form7aController($scope,$http,$compile, $filter) {
    $scope.total = 1299
    $scope.realestate = []
    $scope.brand = []
    $scope.variety = []
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    $scope.pageType = urlParams.get('type')
    $scope.projectId = window.localStorage.getItem('7a_project_id');
    $scope.quarters = [
        {name: 'Quý 1', value: '1'},
        {name: 'Quý 2', value: '2'},
        {name: 'Quý 3', value: '3'},
        {name: 'Quý 4', value: '4'},
    ]
    $scope.projectInfo = {
        name: '',
        code: '',
        scale: '',
        total_investment: 0,
        type_of_investment: 1,
        start_date:'',
        end_date: ''
    }
    $scope.compile = $compile
    $scope.baseRow = {
        apr_room: 0,
        apr_square: 0,
        eligible_room: 0,
        eligible_square: 0,
        accum_room: 0,
        accum_square: 0,
        real_estate_id: 1,
        variety_id: 1,
        brand_id: 1,
        quarter: '',
        year: ''
    }
    $scope.getCookiebyName = function getCookie(name) {
        // Split cookie string and get all individual name=value pairs in an array
        var cookieArr = document.cookie.split(";");

        // Loop through the array elements
        for(var i = 0; i < cookieArr.length; i++) {
            var cookiePair = cookieArr[i].split("=");

            /* Removing whitespace at the beginning of the cookie name
            and compare it with the given string */
            if(name == cookiePair[0].trim()) {
                // Decode the cookie value and return
                return decodeURIComponent(cookiePair[1]);
            }
        }

        // Return null if not found
        return null;
    }
    $scope.composeData = (data)=>{
        var prjInfo = data.info
        $scope.projectInfo = {
            project_id: $scope.projectId,
            name: prjInfo.name,
            code: prjInfo.code,
            scale: prjInfo.scale,
            total_investment: prjInfo.total_investment,
            type_of_investment: prjInfo.type_of_investment,
            start_date:new Date(prjInfo.start_date),
            end_date: new Date(prjInfo.end_date),
        }
        var declares = Object.values(data.declares)
        if(declares.length > 0){
            declares.forEach((elm) => {
                console.log(elm)
                if(! elm.eligible_room && ! elm.eligible_square){
                    $scope.listAcceptBds.push(elm)
                }else{
                    $scope.listTranBds.push(elm)
                }
            });
        }
    }
    $scope.init = async ()=>{
        var userInfo = JSON.parse($scope.getCookiebyName('UserInfo'))
        if(userInfo.group === GROUP_INVESTOR){
            $scope.type = 'investor'
            // get project for investor
        }else{
            $scope.type = 'bxd'
        }
        if($scope.pageType == 'edit'){
            // get project Info
            await $http.get('/api/internal/projects/item', {params: {
                    project_id: $scope.projectId,
                }}).then((response) => {
                $scope.composeData(response.data)
            }).catch((err) => {
                if(err.status == 401){
                    window.location.href = "/login.html";
                }
            })
        }


        await $http.get('/api/internal/investors/bds', {}).then((response) => {
            $scope.realestate = response.data.realestate
            $scope.brand = response.data.brand
            $scope.variety = response.data.variety
        }).catch((err) => {
           if(err.status == 401){
               window.location.href = "/login.html";
           }
        })

    }
    $scope.listAcceptBds = []
    $scope.listTranBds = []
    if($scope.pageType != 'edit'){
        $scope.listAcceptBds.push(angular.copy($scope.baseRow))
        $scope.listTranBds.push(angular.copy($scope.baseRow))
    }

    $scope.addNew = (type)=>{
        if(type == 'canTran'){
            $scope.listTranBds.push(angular.copy($scope.baseRow))
        }else{
            $scope.listAcceptBds.push(angular.copy($scope.baseRow))
        }

    }
    $scope.deleteRow = (index, type)=>{
        if(type == 'canTran'){
            $scope.listTranBds.splice(index, 1);
        }else{
            $scope.listAcceptBds.splice(index, 1);
        }
    }
    $scope.init()
    $scope.save = ()=>{
        var formData = new FormData();
        formData.append('type', '7a');
        var rowData = $scope.listAcceptBds.concat($scope.listTranBds)
        formData.append('row_data', JSON.stringify(rowData));
        formData.append('project_info', JSON.stringify($scope.projectInfo));
        console.log($scope.projectInfo)
        if($scope.pageType == 'edit'){
            formData.append('is_update', true);
        }
        $http.post('/api/internal/investors', formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then((result) => {
            alert('Lưu Dữ Liệu Thành Công')
            // set user info to cookie
        }).catch((err)=>{
            console.log(err)
        })
    }
    $scope.logout = ()=>{
        var formData = new FormData();
        $http.post('/api/internal/users/log_out', formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then((result) => {
            window.location.href = '/login.html'
        }).catch((err)=>{
            console.log(err)
        })
    }
}
