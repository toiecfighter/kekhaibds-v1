var myApp = angular.module('loginModule', [])
myApp.controller('loginController', loginController)
loginController.$inject = ['$scope', '$http']
function loginController($scope, $http) {
    $scope.total = 1299
    $scope.loginBtn = function (userName, passWord) {
        var formData = new FormData();
        formData.append('user_name', userName);
        formData.append('pass_word', passWord);
        $http.post('/api/internal/users/login', formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then((result) => {
            // set user info to cookie
            document.cookie = 'UserInfo' + "=" + JSON.stringify(result.data);
            window.location.href = "/";
        }).catch((err)=>{
            console.log(err)
        })
        console.log(userName,passWord)

    }
}
