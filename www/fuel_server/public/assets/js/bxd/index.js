var myApp = angular.module('bxdModule', [])
myApp.controller('bxdController', bxhController)
bxhController.$inject = ['$scope', '$http']
function bxhController($scope,$http) {
    $scope.total = 1299
    var req = {
        method: 'GET',
        url: '/test/list',
        headers: {
            'Content-Type': undefined
        },
    }
    $http({
        url: '/test/list',
        method:'GET',
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        params: {
            query:'Hello World'
        }

    })
    .then(function (response) {
        console.log(response)
    }).catch(()=>{
        console.log(2)
    });
}
