const GROUP_INVESTOR = 1
const GROUP_BXD = 2
var myApp = angular.module('registerModule', [])
myApp.controller('registerController', registerController)
registerController.$inject = ['$scope', '$http']
function registerController($scope, $http) {
    $scope.listData = []
    $scope.init = async ()=>{
        $scope.filterParams = {}
        // get user data from cookie
        var userInfo = JSON.parse($scope.getCookiebyName('UserInfo'))
        if(!userInfo){
            window.location.href = "/login.html";
        }
        if(userInfo.group === GROUP_INVESTOR){
            $scope.type = 'investor'
            // get project for investor
        }else if(userInfo.group === GROUP_BXD){
            $scope.type = 'bxd'
            // get list soxd
            await $scope.getListSoXD();
        }else{
            $scope.type = 'soxd'
            await $scope.getListCdt()
        }

    }

    $scope.getListSoXD = async ()=>{
        await $http.get('/api/internal/soxds', {}).then((response) => {
            $scope.listData = response.data
        }).catch((err) => {
            if(err.status == 401){
                window.location.href = "/login.html";
            }
        })
    }
    $scope.getListCdt = async ()=>{
        await $http.get('/api/internal/investors', {}).then((response) => {
            $scope.listData = response.data
        }).catch((err) => {
            if(err.status == 401){
                window.location.href = "/login.html";
            }
        })
    }

    $scope.addNewUser = (type)=>{
        //bxd => add for soxd
        if($scope.type == 'bxd'){
            var formData = new FormData();
            formData.append('name', $scope.filterParams.name);
            formData.append('city', $scope.filterParams.city);
            formData.append('address', $scope.filterParams.address);
            formData.append('email', $scope.filterParams.email);
            formData.append('user_name', $scope.filterParams.user_name);
            formData.append('pass_word', $scope.filterParams.pass_word);
            $http.post('/api/internal/soxds', formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(async (result) => {
                alert('Lưu Dữ Liệu Thành Công')
                $scope.filterParams = {}
                await $scope.getListSoXD();
            }).catch((err)=>{
                alert(err)
            })
        }else{
            var formData = new FormData();
            formData.append('name', $scope.filterParams.name);
            formData.append('tax_code', $scope.filterParams.name);
            var userInfo = JSON.parse($scope.getCookiebyName('UserInfo'))
            formData.append('city', userInfo.city ? userInfo.city : 'Hà Nội');
            formData.append('address', $scope.filterParams.address);
            formData.append('email', $scope.filterParams.email);
            formData.append('user_name', $scope.filterParams.user_name);
            formData.append('pass_word', $scope.filterParams.pass_word);
            $http.post('/api/internal/investors/account', formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(async (result) => {
                $scope.filterParams = {}
                await $scope.getListCdt();
            }).catch((err)=>{
                alert(err)
            })
        }
    }
    $scope.getCookiebyName = function getCookie(name) {
        // Split cookie string and get all individual name=value pairs in an array
        var cookieArr = document.cookie.split(";");

        // Loop through the array elements
        for(var i = 0; i < cookieArr.length; i++) {
            var cookiePair = cookieArr[i].split("=");

            /* Removing whitespace at the beginning of the cookie name
            and compare it with the given string */
            if(name == cookiePair[0].trim()) {
                // Decode the cookie value and return
                return decodeURIComponent(cookiePair[1]);
            }
        }

        // Return null if not found
        return null;
    }
    $scope.init()
    $scope.logout = ()=>{
        var formData = new FormData();
        $http.post('/api/internal/users/log_out', formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then((result) => {
            window.location.href = '/login.html'
        }).catch((err)=>{
            console.log(err)
        })
    }

}
