var myApp = angular.module('cardsModule', [])
myApp.controller('cardsController', cardsController)
cardsController.$inject = ['$scope', '$http']
function cardsController($scope, $http) {
    $scope.declareData = []
    $scope.transactionData = []
    $scope.rowData = {}
    $scope.listTran = []
    $scope.tranAllRoom = 0
    $scope.tranAllSquare = 0
    $scope.projectId = window.localStorage.getItem('project_id');
    $scope.filterParams = window.localStorage.getItem('filterParams');
    $scope.getData = async ()=>{
        // get data from local storage
        var project_id = window.localStorage.getItem('project_id');
        var filterParams = JSON.parse(window.localStorage.getItem('filterParams'));
        filterParams.project_id = project_id
        await $http.get('/api/internal/projects/declare_data', {params: filterParams}).then((response) => {
            $scope.rowData = response.data
        }).catch((err) => {
            if(err.status == 401){
                window.location.href = "/login.html";
            }
        })

        // get declare
        await $http.get('/api/internal/declares', {params: filterParams}).then((response) => {
            $scope.declareData = response.data
        }).catch((err) => {
            if(err.status == 401){
                window.location.href = "/login.html";
            }
        })

        await $http.get('/api/internal/transactions', {params: filterParams}).then((response) => {
            $scope.transactionData = response.data
            $scope.getListTranByCustomId(1,1,1)
        }).catch((err) => {
            if(err.status == 401){
                window.location.href = "/login.html";
            }
        })
    }
    $scope.getAprDataRoom = (real_estate_id)=>{
        var total = 0
        $scope.declareData.forEach((element) => {
            if(element.real_estate_id == real_estate_id)
                total += (element.apr_room ? parseInt(element.apr_room) : 0)
        });
        return total
    }
    $scope.getAprDataSquare = (real_estate_id)=>{
        var total = 0
        $scope.declareData.forEach((element) => {
            if(element.real_estate_id == real_estate_id)
                total += (element.apr_square ? parseInt(element.apr_square) : 0)
        });
        return total
    }
    $scope.getEllRoom = (real_estate_id, brand_id, variety_id)=>{
        var total = 0

        $scope.declareData.forEach((element) => {
            var condition = true
            if(variety_id){
                condition = element.variety_id == variety_id
            }
            if(element.real_estate_id == real_estate_id &&
                element.brand_id == brand_id &&
                condition)
                total += (element.eligible_room ? parseInt(element.eligible_room) : 0)
        });
        return total
    }
    $scope.getEllSquare = (real_estate_id, brand_id, variety_id)=>{
        var total = 0
        $scope.declareData.forEach((element) => {
            var condition = true
            if(variety_id){
                condition = element.variety_id == variety_id
            }
            if(element.real_estate_id == real_estate_id &&
                element.brand_id == brand_id &&
                condition)
                total += (element.eligible_square ? parseInt(element.eligible_square) : 0)
        });
        return total
    }
    $scope.getListTranByCustomId = (real_estate_id, brand_id, variety_id)=>{
        $scope.transactionData.filter((element) => {
            console.log(1)
            if(element.real_estate_id == real_estate_id &&
                element.brand_id == brand_id && element.variety_id == variety_id){
                $scope.tranAllRoom+= (element.room ? parseInt(element.room) : 0)
                $scope.tranAllSquare+= (element.square ? parseInt(element.square) : 0)
                $scope.listTran.push(element)
            }
        });
    }
    $scope.getData()
}
