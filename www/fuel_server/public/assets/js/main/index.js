const GROUP_INVESTOR = 1
const GROUP_BXD = 2
var myApp = angular.module('mainModule', [])
myApp.controller('mainController', mainController)
mainController.$inject = ['$scope', '$http']
function mainController($scope, $http) {
    $scope.AtotalRoom = 0
    $scope.AtotalSquare = 0
    $scope.EtotalRoom = 0
    $scope.EtotalSquare = 0
    $scope.totalTranRoom = 0
    $scope.totalTranSquare = 0
    $scope.totalPrice = 0
    $scope.listPrj = []
    $scope.type = ''
    $scope.filterParams = {
        quarter: '1',
        year: '2021',
        city: ''
    }
    $scope.soxdName = ''
    $scope.bdsData = []
    // init data

    $scope.init = async ()=>{

        // get user data from cookie
        var userInfo = JSON.parse($scope.getCookiebyName('UserInfo'))
        if(!userInfo){
            window.location.href = "/login.html";
        }
        if(userInfo.group === GROUP_INVESTOR){
            $scope.type = 'investor'
            // get project for investor
        }else if(userInfo.group === GROUP_BXD){
            $scope.type = 'bxd'
        }else{
            $scope.type = 'soxd'
            $scope.filterParams.city = userInfo.city
            $scope.soxdName = userInfo.name
            console.log($scope.soxdName)
        }

        $scope.search()
    }
    $scope.getCookiebyName = function getCookie(name) {
        // Split cookie string and get all individual name=value pairs in an array
        var cookieArr = document.cookie.split(";");

        // Loop through the array elements
        for(var i = 0; i < cookieArr.length; i++) {
            var cookiePair = cookieArr[i].split("=");

            /* Removing whitespace at the beginning of the cookie name
            and compare it with the given string */
            if(name == cookiePair[0].trim()) {
                // Decode the cookie value and return
                return decodeURIComponent(cookiePair[1]);
            }
        }

        // Return null if not found
        return null;
    }

    $scope.logout = ()=>{
        var formData = new FormData();
        $http.post('/api/internal/users/log_out', formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then((result) => {
            window.location.href = '/login.html'
        }).catch((err)=>{
            console.log(err)
        })
    }
    $scope.search = async ()=>{
        $scope.EtotalRoom = 0
        $scope.EtotalSquare = 0
        $scope.totalTranRoom = 0
        $scope.totalTranSquare = 0
        $scope.totalPrice = 0
        var params = {}
        if($scope.filterParams.city){
            params.city = $scope.filterParams.city
        }
        await $http.get('/api/internal/investors/projects', {params: params}).then((response) => {
            $scope.listPrj = response.data
        }).catch((err) => {
            if(err.status == 401){
                window.location.href = "/login.html";
            }
        })
        await $http.get('/api/internal/declares', {params: $scope.filterParams}).then((response) => {
            response.data.forEach((dat) => {
                $scope.EtotalRoom += (dat.eligible_room ? parseInt(dat.eligible_room) : 0)
                $scope.EtotalSquare += (dat.eligible_square ? parseInt(dat.eligible_square) : 0)
            });
        }).catch((err) => {
            if(err.status == 401){
                window.location.href = "/login.html";
            }
        })
        await $http.get('/api/internal/transactions', {params: $scope.filterParams}).then((response) => {
            response.data.forEach((dat) => {
                $scope.totalTranRoom += (dat.room ? parseInt(dat.room) : 0)
                $scope.totalTranSquare += (dat.square ? parseInt(dat.square) : 0)
                $scope.totalPrice+= parseInt(dat.square)*parseInt(dat.price) + parseInt(dat.room)*parseInt(dat.price)
            });
        }).catch((err) => {
            if(err.status == 401){
                window.location.href = "/login.html";
            }
        })
    }
    $scope.handleResult = (project_id)=>{
        console.log(window.location.host)
        // set data to local storage
        localStorage.setItem("project_id", project_id);
        localStorage.setItem("filterParams", JSON.stringify($scope.filterParams));
        window.open('/cards.html', '_blank');
    }
    $scope.handleUpdate = (project_id)=>{
        // set data to local storage
        localStorage.setItem("7a_project_id", project_id);
        window.location.href = "/forms.html?type=edit";
    }

    $scope.handleTrans = (project_id)=>{
        // set data to local storage
        localStorage.setItem("7c_project_id", project_id);
        window.location.href = "/forms-7c.html?type=edit";
    }

    $scope.registerAccount = ()=>{
        window.location.href = "/register.html";
    }
    $scope.init()
}
