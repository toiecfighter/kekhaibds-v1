const GROUP_INVESTOR = 1
const GROUP_BXD = 2
var myApp = angular.module('form7cModule', [])
myApp.controller('form7cController', form7cController)
form7cController.$inject = ['$scope', '$http', '$compile']
function form7cController($scope,$http,$compile) {
    $scope.quarters = [
        {name: 'Quý 1', value: '1'},
        {name: 'Quý 2', value: '2'},
        {name: 'Quý 3', value: '3'},
        {name: 'Quý 4', value: '4'},
    ]
    $scope.projectId = 0
    $scope.tranType = 0
    $scope.listTrans = []
    $scope.baseRow = {
        buyer: '',
        type: 0,
        square: 0,
        price: 0,
        room: 0,
        real_estate_id: 1,
        variety_id: 1,
        brand_id: 1,
        quarter: 0,
        year: 0
    }
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    $scope.pageType = urlParams.get('type')
    $scope.projectId = window.localStorage.getItem('7c_project_id');
    $scope.getCookiebyName = function getCookie(name) {
        // Split cookie string and get all individual name=value pairs in an array
        var cookieArr = document.cookie.split(";");

        // Loop through the array elements
        for(var i = 0; i < cookieArr.length; i++) {
            var cookiePair = cookieArr[i].split("=");

            /* Removing whitespace at the beginning of the cookie name
            and compare it with the given string */
            if(name == cookiePair[0].trim()) {
                // Decode the cookie value and return
                return decodeURIComponent(cookiePair[1]);
            }
        }

        // Return null if not found
        return null;
    }
    $scope.addNew = ()=>{
        $scope.listTrans.push(angular.copy($scope.baseRow))
    }
    $scope.deleteRow = (index)=>{
        $scope.listTrans.splice(index, 1);
    }
    $scope.save = ()=>{
        var formData = new FormData();
        formData.append('project_id', $scope.projectId);
        formData.append('row_data', JSON.stringify($scope.listTrans));
        if($scope.pageType == 'edit'){
            formData.append('is_update', true);
        }
        $http.post('/api/internal/transactions', formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then((result) => {
            alert('Lưu Dữ Liệu Thành Công')
            // set user info to cookie
        }).catch((err)=>{
            console.log(err)
        })
    }
    if($scope.pageType != 'edit'){
        $scope.listTrans.push(angular.copy($scope.baseRow))
    }

    $scope.init = async ()=>{
        var userInfo = JSON.parse($scope.getCookiebyName('UserInfo'))
        if(userInfo.group === GROUP_INVESTOR){
            $scope.type = 'investor'
            // get project for investor
        }else{
            $scope.type = 'bxd'
        }
        if($scope.pageType == 'edit'){
            await $http.get('/api/internal/transactions', {params:{
                    project_id: $scope.projectId,
                }}).then((response) => {
                var tranData = response.data
                console.log(response.data)
                tranData.forEach((tran) =>{
                    $scope.listTrans.push(tran)
                });
            }).catch((err) => {
                if(err.status == 401){
                    window.location.href = "/login.html";
                }
            })
        }


        await $http.get('/api/internal/investors/projects', {}).then((response) => {
            $scope.listPrj = response.data
        }).catch((err) => {
            if(err.status == 401){
                window.location.href = "/login.html";
            }
        })
        await $http.get('/api/internal/investors/bds', {}).then((response) => {
            $scope.realestate = response.data.realestate
            $scope.brand = response.data.brand
            $scope.variety = response.data.variety
        }).catch((err) => {
            if(err.status == 401){
                window.location.href = "/login.html";
            }
        })
    }
    $scope.logout = ()=>{
        var formData = new FormData();
        $http.post('/api/internal/users/log_out', formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then((result) => {
            window.location.href = '/login.html'
        }).catch((err)=>{
            console.log(err)
        })
    }
    $scope.init()
}
